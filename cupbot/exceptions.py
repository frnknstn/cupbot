"""module-wide Exception definitions"""
from logging import error 
import traceback
from typing import Dict, Any

import flask
import werkzeug


# exception classes
class CupbotException(Exception):
    pass


class AuthenticationError(CupbotException):
    pass


class GameError(CupbotException):
    pass


class GameOverError(GameError):
    pass


class BetError(GameError):
    pass


# helper functions
def error_response(msg, code: int = 400, data: Dict[str, Any] = None):
    """Error return helper, for sending custom errors from user code"""
    error(f"Error response: {code} - {msg}")
    retval = {"msg": msg, "code": code}
    if data:
        retval.update(data)
    return flask.jsonify(retval), code


def generic_exception_handler(msg, code):
    response = error_response(msg, code)
    error("A generic exception occurred! Returning 500 Server Error")
    traceback.print_exc()
    return response


# register our exception handlers
def register_exception_callbacks(app):
    @app.errorhandler(AuthenticationError)
    def handle_bad_auth(error):
        """Unhanded errors in twitch code"""
        traceback.print_exc()
        return generic_exception_handler(str(error), 400)

    @app.errorhandler(CupbotException)
    def handle_cupbot_error(error):
        """Fallback for cupbot exceptions"""
        traceback.print_exc()
        return generic_exception_handler(str(error), 400)

    @app.errorhandler(werkzeug.exceptions.BadRequest)
    def handle_bad_request(error):
        """Unhanded errors in library code"""
        traceback.print_exc()
        return generic_exception_handler(str(error), 400)

    @app.errorhandler(werkzeug.exceptions.NotFound)
    def handle_request_routing_exception(error):
        """This endpoint doesn't exist"""
        traceback.print_exc()
        return generic_exception_handler("The requested URL was not found on the server.", 404)

    @app.errorhandler(Exception)
    def handle_generic_exception(error):
        """A generic unhandled exception.

        This handler will result in a generic 500 error being sent to the client, but still
        print a full traceback in the log file to assist debugging.
        """
        return generic_exception_handler("Internal server error", 500)

