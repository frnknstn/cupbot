import random
import time

import sqlalchemy
import sqlalchemy.ext.declarative

from sqlalchemy import Column, String, DateTime

db_now = sqlalchemy.func.now

Model = sqlalchemy.ext.declarative.declarative_base()


# So we can change SIGTERM to an exception 
class SigTermInterrupt(KeyboardInterrupt):
    pass


def configure_sigterm():
    """change console window close event to be an exception rather than just killing the interpreter"""
    import signal

    def sigterm_handler(sig, frame=None):
        raise SigTermInterrupt()

    signal.signal(signal.SIGTERM, sigterm_handler)


class Cooldown(Model):
    __tablename__ = "cooldown"

    user_id = Column("user_id", String, primary_key=True)
    context = Column("context", String, primary_key=True)
    date_created = Column("date_created", DateTime, default=db_now)
    expires = Column("expires", DateTime, nullable=False)


def make_database(filename="test.sqlite", echo=True, unsafe=False):
    db_path = "sqlite:///" + filename
    print("opening " + db_path)
    engine = sqlalchemy.create_engine(db_path, echo=echo)
    Model.metadata.create_all(engine)
    
    # unsafe mode
    if unsafe:
        @sqlalchemy.event.listens_for(sqlalchemy.engine.Engine, "connect")
        def sqlite_pragma_event(dbapi_connection, connection_record):
            cursor = dbapi_connection.cursor()
            cursor.execute("PRAGMA synchronous=OFF;")
            cursor.execute("PRAGMA journal_mode=WAL;")
            #cursor.execute("PRAGMA journal_mode=MEMORY;")
            cursor.close()

    return engine
    

def apply_cooldown(conn):
    user_id = random.randint(1, 1000)
    context = "cooldown"
    seconds = random.randint(1, 10)
    
    expires = "datetime('now') + '%f seconds'" % seconds
    
    conn.execute(
        f"delete from cooldown where user_id = {user_id} and context = '{context}';"
    )
    conn.execute(
        f"insert into cooldown (user_id, context, date_created, expires)"
        f"values ({user_id}, '{context}', datetime('now'), {expires});"
    )


def main():
    configure_sigterm()
    
    # db = make_database("test.sqlite", echo=False) # 225
    db = make_database("test.sqlite", echo=False, unsafe=True)  # 931 6624 4150
    # db = make_database("d:\\test.sqlite", echo=False)  # 2.5
    # db = make_database("d:\\test.sqlite", echo=False, unsafe=True)  # 60 6548 5256
    conn = db.connect()

    count = 0
    start_time = time.time()
    try:
        while True:
            with conn.begin():
                apply_cooldown(conn)
            count += 1
        
            if count % 1000 == 0:
                print(count)
    finally:
        duration = time.time() - start_time
        print(f"{count} transactions in {duration} seconds, {count / duration}/sec")
        

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Got KeyboardInterrupt")
    finally:
        print("ending.")