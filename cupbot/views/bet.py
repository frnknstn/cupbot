from logging import debug, info, warning

import flask
from flask import request

from cupbot.database import db, NoResultFound

from cupbot.exceptions import error_response, GameError

from cupbot.models.bet import Player, Bet
from cupbot.models.game import Game
from cupbot.models.cooldown import check_cooldown
from cupbot.twitch import twitch_ext

blueprint = flask.Blueprint('bet', __name__)


BET_COOLDOWN_SEC = 3.0


# helper func
def bet_cooldown(player: Player):
    """Cooldown is per player and refreshes at the start of a leg"""
    return check_cooldown(player.twitch_opaque_id, "bet-leg-"+str(player.game.current_leg.leg_index), BET_COOLDOWN_SEC)    


@blueprint.route("/pick/<any('leg','race'):route_category>", methods=["POST"])
def bet_leg(route_category):
    """bet on which camel will win the leg"""
    payload = twitch_ext.verify_and_decode(request)
    channel_id = payload["channel_id"]
    opaque_user_id = payload["opaque_user_id"]
    user_id = payload.get("user_id", None)
    
    # get / create the player
    game = Game.for_channel(channel_id)
    player = Player.get_or_create(opaque_user_id, game, user_id)
    
    # check our bet
    cooldown = bet_cooldown(player)
    if cooldown > 0:
        return error_response(
            f"option is on cooldown for {cooldown:.3} sec", 429, data={"pick_cooldown":cooldown}
        )

    json_data = request.get_json(force=True)
    camel = json_data["winner"]

    if route_category == "leg":
        category = "leg win"
    elif route_category == "race":
        category = "race win"
    else:
        return error_response("invalid category", 404)
    
    info(f"Bet for *{camel} 🐪* {category} in {game} by {player}")
    
    # place our bet
    bet = Bet(player, category, camel)
    db.session.add(bet)
    db.session.commit()
    
    retval = player.for_json()
    if cooldown == 0:
        cooldown = BET_COOLDOWN_SEC
    retval["pick_cooldown"] = cooldown
    
    return flask.jsonify(retval)


@blueprint.route("/player/query", methods=["GET"])
def player_query():
    """Return the active player details, if they exist"""
    payload = twitch_ext.verify_and_decode(request)
    channel_id = payload["channel_id"]
    opaque_user_id = payload["opaque_user_id"]

    # get / create the player
    game = Game.for_channel(channel_id)
    try:
        player = Player.get_player(opaque_user_id, game)
    except NoResultFound:
        return error_response("no player yet", 400)
    else:
        return flask.jsonify(player.for_json())
