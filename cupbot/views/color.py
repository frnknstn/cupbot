from logging import debug, info
import random

import flask
from flask import request

from cupbot.database import db

from cupbot.models.color import Color
from cupbot.models.cooldown import check_cooldown
from cupbot.twitch import twitch_ext

blueprint = flask.Blueprint('color', __name__)


@blueprint.route("/color/cycle", methods=["POST"])
def color_cycle():
    info("color_cycle()")
    
    payload = twitch_ext.verify_and_decode(request)
    info("Payload: " + str(payload))

    # user cooldown    
    user_id = payload["opaque_user_id"]

    if check_cooldown(user_id, "color_cycle", 2) > 0:
        # return 429 TOO MANY REQUESTS
        info(f"Not resetting color for {user_id}, on cooldown")
        c = Color.get_color()
        return c.get_hex().encode("utf8"), 429

    # change the color
    c = Color.set_color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    color_code = c.get_hex()

    twitch_ext.send_pubsub_broadcast(payload["channel_id"], color_code)
    
    # real user?
    if "user_id" in payload:
        info("checking user details")
        real_user_id = payload["user_id"]
        user = twitch_ext.get_user_details(real_user_id)
        info(user)
    
    return color_code.encode("utf8")


@blueprint.route("/color/query", methods=["GET"])
def color_query():
    debug("color_query()")

    c = Color.get_color()
    return c.get_hex().encode("utf8")

    payload = twitch_ext.verify_and_decode(request)
    info("Payload: " + str(payload))

    c = Color.get_color()
    return c.get_hex().encode("utf8")

