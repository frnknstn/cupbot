from logging import debug, info, warning

import flask
from flask import request

from cupbot.database import db

from cupbot.exceptions import error_response, GameError

from cupbot.models.game import Game, Leg
from cupbot.twitch import twitch_ext

blueprint = flask.Blueprint('game', __name__)

@blueprint.route("/game/create", methods=["POST"])
def game_create():
    """Create a new game for a channel"""
    info("game_create()")
    
    # TODO: verify
    payload = twitch_ext.verify_and_decode(request)
    channel_id = payload["channel_id"]

    # TODO: end any existing game?
    try:
        game = Game.for_channel(channel_id)
    except GameError:
        # no other active game
        pass
    else:
        warning(f"There is already an active game for channel {channel_id} ({game})")
    
    # make a new game
    game = Game.create(channel_id, "1st")
    
    info(f"created game {game}")
    info(game.for_json())
    
    return flask.jsonify(game.for_json())


@blueprint.route("/game/leg", methods=["POST"])
def game_leg():
    """Advance a game to the next leg, or end it"""

    # TODO: verify
    payload = twitch_ext.verify_and_decode(request)
    channel_id = payload["channel_id"]
    
    game = Game.for_channel(channel_id)

    json_data = request.get_json(force=True)
    game.end_leg(json_data["winner"], json_data["second"], json_data["loser"], json_data["final"])
    
    # TODO: add up the bets
    
    return flask.jsonify(game.for_json())


@blueprint.route("/game/query", methods=["GET"])
def game_query():
    """Get the current game stats"""

    payload = twitch_ext.verify_and_decode(request)
    channel_id = payload["channel_id"]
    game = Game.for_channel(channel_id, active=False)

    return flask.jsonify(game.for_json())

