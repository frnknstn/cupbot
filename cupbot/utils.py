"""Useful utility functions"""

import os
import errno
import datetime


_now = datetime.datetime.now


def now():
    """return a timezone-aware timestamp for the system's timezone"""
    return _now().astimezone()


