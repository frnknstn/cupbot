"""CupBot - Camel Up Twitch extention backend for user participation"""

import sys
import datetime
import logging
import logging.handlers
from logging import info

import flask
from flask.json import JSONEncoder
import flask_cors

import cupbot.database


__version__ = "0.0.1"


# Replace the default JSON encoder
from cupbot import exceptions


class CustomJSONEncoder(JSONEncoder):
    """Custom JSON encoder class with support for other types, e.g. dates"""
    def default(self, obj):
        if isinstance(obj, datetime.date):
            return obj.isoformat(' ')

        # default case
        return JSONEncoder.default(self, obj)


def configure_logging(level=logging.INFO):
    """Set up the logging module to suit our needs"""

    log_format = "%(asctime)s %(threadName)s: %(message)s"
    log_file = (__name__ + ".log")
    
    # log to stdout
    import codecs
    logging.basicConfig(level=level,
                        format=log_format)

    # log to file
    handler = logging.handlers.RotatingFileHandler(log_file, encoding='utf-8', maxBytes=2*1024*1024, backupCount=9)
    handler.setLevel(level)
    handler.setFormatter(logging.Formatter(log_format))
    logging.getLogger('').addHandler(handler)

    # log uncaught exceptions
    sys.excepthook = lambda x, y, z: logging.critical("Unhandled exception:", exc_info=(x, y, z))

    logging.info("logging to file '%s'" % log_file)


def register_blueprints(app):
    import cupbot.views.color
    app.register_blueprint(cupbot.views.color.blueprint)
    import cupbot.views.game
    app.register_blueprint(cupbot.views.game.blueprint)
    import cupbot.views.bet
    app.register_blueprint(cupbot.views.bet.blueprint)


def create_app():
    """Flask app factory"""
    configure_logging(level=logging.DEBUG)
    
    info("Creating app for %s v%s" % (__name__, __version__))

    # TODO: Disable this developer mode
    import os
    #os.environ["FLASK_ENV"] = "development"
    
    app = flask.Flask(__name__)
    app.json_encoder = CustomJSONEncoder
    flask_cors.CORS(app)
           
    # configure our app
    app.config.update(dict(
        POPULATE_DB=False,
        SQLALCHEMY_TRACK_MODIFICATIONS=False,  # SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and will be disabled by default in the future
        SQLALCHEMY_DATABASE_URI="sqlite:///cupbot.sqlite",
        SQLALCHEMY_ECHO=False,
    ))
    app.config.from_pyfile('cupbot.cfg', silent=False)

    # import blueprints and views
    register_blueprints(app)
    
    # set up persistent storage
    database.init_database(app)
    
    import cupbot.twitch
    cupbot.twitch.configure(
        client_id=app.config["TWITCH_CLIENT_ID"],
        jwt_secret=app.config["TWITCH_JWT_SECRET"],
        owner_id=app.config["TWITCH_OWNER_ID"],
        api_access_token=app.config["TWITCH_API_ACCESS_TOKEN"]
    )

    exceptions.register_exception_callbacks(app)
    
    return app
