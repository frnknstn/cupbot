from logging import info, debug
from datetime import datetime, timezone

import sqlalchemy
import flask_sqlalchemy
from flask_sqlalchemy import SQLAlchemy

# useful exceptions
from sqlalchemy.exc import IntegrityError
NoResultFound = flask_sqlalchemy.orm.exc.NoResultFound
MultipleResultsFound = flask_sqlalchemy.orm.exc.MultipleResultsFound

db = SQLAlchemy()

# yum102


class TimeStampTZ(sqlalchemy.types.TypeDecorator):
    """datetime helper with timezones.
    
    Based on https://mike.depalatis.net/blog/sqlalchemy-timestamps.html
    
    Since the Sqlite implementation just strips time zones from datetime objects, we force all 
    datetimes to be stored in UTC in the database, and then present them as the local timezone 
    to the user.
    """
    impl = sqlalchemy.types.DateTime
    LOCAL_TIMEZONE = datetime.now().astimezone().tzinfo

    def process_bind_param(self, value: datetime, dialect):
        if value is None:
            return None
        if value.tzinfo is None:
            value = value.astimezone(self.LOCAL_TIMEZONE)
        return value.astimezone(timezone.utc)

    def process_result_value(self, value, dialect) -> datetime:
        if value is None:
            return None
        if value.tzinfo is None:
            return value.replace(tzinfo=timezone.utc).astimezone(self.LOCAL_TIMEZONE)
        return value.astimezone(self.LOCAL_TIMEZONE)
    
    def process_literal_param(self, value, dialect):
        return value


def init_database(app):
    db.init_app(app)
    
    # TODO: create rolling backups of the database
    
    @sqlalchemy.event.listens_for(sqlalchemy.engine.Engine, "connect")
    def sqlite_pragma_event(dbapi_connection, connection_record):
        """Set all sqlite connections to the unsafe fast mode"""
        cursor = dbapi_connection.cursor()
        cursor.execute("PRAGMA synchronous=OFF;")
        cursor.execute("PRAGMA journal_mode=WAL;")
        cursor.close()

    if app.config["POPULATE_DB"]:
        @app.before_first_request
        def populate_db():
            # create tables etc
            conn = db.engine.connect()
            with conn.begin():
                info("removing any existing tables")
                db.drop_all()
                info("creating tables")
                db.create_all()
                info("populating tables")
                conn.execute("insert into color values (0, 0, 0);")
                # conn.execute("INSERT INTO game (game_id, channel_id, edition, start_date, end_date) VALUES (1, '45248144', '1st', '2020-09-28 22:14:55.001', null);")
                # conn.execute("INSERT INTO leg (game_id, leg_index, start_date, end_date, winner, loser) VALUES (1, 1, '2020-10-01 20:49:37.776854', null, null, null);")