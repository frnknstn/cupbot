"""Game state etc"""
import math
from logging import info, debug
from datetime import datetime
from typing import Iterable, Set

from sqlalchemy import Column, String, Integer

import cupbot.database
import cupbot.utils
from cupbot.database import db, TimeStampTZ, NoResultFound
from cupbot.exceptions import GameError, GameOverError
import cupbot.models.bet

# consts and such
EDITIONS = (
    "1st",
    "2nd",
)

CAMEL_COLORS_1ST_ED = (
    "blue", #"#6A84C1"
    "green", #"#6AD577"
    "orange", #"#FF957F"  # 1st ed
    "yellow", #"#F7FD7E"
    "white",  #"#6C6C6C"  # 1st ed
)

CAMEL_CODES_1ST_ED = (
    # "blue", 
    "#6A84C1",
    # "green", 
    "#6AD577",
    # "orange", 
    "#FF957F",  # 1st ed
    # "yellow", 
    "#F7FD7E",
    # "white",  
    "#6C6C6C"  # 1st ed
)
CAMEL_COLORS_2ND_ED = (
    "blue",
    "green",
    "red",      # 2nd ed
    "yellow",
    "purple",   # 2nd ed
)

WINNER_TIERS = (8, 5, 3, 2, 1)


# classes
class Game(db.Model):
    """A game of Camel Cup"""
    __tablename__ = "game"
    
    game_id = Column("game_id", Integer, primary_key=True)
    channel_id = Column("channel_id", String, nullable=False)
    
    edition = Column("edition", String, nullable=False)
    
    start_date = Column("start_date", TimeStampTZ, nullable=False)
    end_date = Column("end_date", TimeStampTZ, nullable=True)
    
    player_count = Column("player_count", Integer, nullable=False, default=0)
    
    def __init__(self, channel_id: str, edition: str):
        """Make a new game object"""
        debug("creating new game object")
        if edition not in EDITIONS:
            raise ValueError("Unknown game edition")
        
        now = cupbot.utils.now()
        
        self.channel_id = channel_id
        self.edition = edition
        self.start_date = now
        self.player_count = 0
        
        self.legs = []  # type: list[Leg]

    @classmethod        
    def create(cls, channel_id: str, edition: str)-> "Game":
        """Kick off a new game"""
        game = cls(channel_id, edition)
        db.session.add(game)
        db.session.flush()
        db.session.add(Leg(game))
        db.session.commit()        
        return game
    
    @classmethod
    def for_channel(cls, channel_id: str, active: bool = True) -> "Game":
        """Return the last active game on a channel
        
        If active is True, raise GameOverError if the game is finished.
        """
        game = cls.query.filter(db.and_(
            cls.channel_id == channel_id,
        )).order_by(cls.game_id.desc()).first()
        
        if not game:
            raise GameError("No game in progress for channel")

        if game.finished and active:
            raise GameOverError("Game for channel is already over")
        
        return game
    
    @property
    def camel_colors(self):
        if self.edition == "1st":
            return CAMEL_COLORS_1ST_ED
        elif self.edition == "2nd":
            return CAMEL_COLORS_2ND_ED
        else:
            raise ValueError("Unknown game edition")
    
    @property
    def current_leg(self) -> "Leg":
        return self.legs[-1]
    
    @property 
    def finished(self):
        return bool(self.end_date)
        
    def for_json(self):
        return {
            "game_id": self.game_id,
            "channel_id": self.channel_id,
            "start_date": self.start_date,
            "end_date": self.end_date,
            "camel_colors": self.camel_colors,
            "current_leg": self.legs[-1].for_json(),
            "edition": self.edition,
            "finished": self.finished,
            "player_count": self.player_count,
        }
        
    def get_final_winner(self):
        """The winner of the entire race, or None"""
        if not self.finished:
            return None
        else:
            return self.current_leg.winner
        
    def get_final_loser(self):
        """The last place finisher of the entire race, or None"""
        if not self.finished:
            return None
        else:
            return self.current_leg.loser
        
    def end_leg(self, winner: str, second: str, loser: str, final: bool):
        
        # mark the leg as done, cut off bets
        now = cupbot.utils.now()
        curr_leg = self.current_leg
        curr_leg.end_date = now
        db.session.flush()
        
        # update the results
        curr_leg.winner = winner
        curr_leg.second = second
        curr_leg.loser = loser
        
        # calculate leg winnings
        for bet in (x for x in self.get_all_bets() if x.category == "leg win"):
            if bet.camel == winner:
                bet.player.money += bet.payout
            elif bet.camel == second:
                bet.player.money += 1
            else:
                bet.player.money -= 1

        # post-leg
        if final:
            self.end_date = now
            self.calc_race_winnings(winner)
            info("game is now over!")
        else:
            # start the next leg
            self.legs.append(Leg(self, curr_leg.leg_index + 1, now))

        # can't go below zero overall
        for player in self.players:
            if player.money < 0:
                player.money = 0
       
        db.session.commit()
        
    def calc_race_winnings(self, winner: str):
        """divide the race winner chits into tiers and pay out."""
        bets = list(x for x in self.get_all_bets() if x.category == "race win")
        
        # find the winning bets
        winning_bets = []   # list: Bet
        for bet in bets:
            if bet.camel == winner:
                winning_bets.append(bet)
            else:
                bet.player.money -= 1

        # split into tiers
        winning_bets.sort(key=lambda x: x.order)
        
        if len(winning_bets) <= len(WINNER_TIERS):
            # base case
            for i, bet in enumerate(winning_bets):
                bet.player.money += WINNER_TIERS[i]
        else:
            # hard case, be generous
            q, r = divmod(len(winning_bets), len(WINNER_TIERS))
            start = 0
            for i, tier in enumerate(WINNER_TIERS):
                size = q + 1 if i < r else q
                bucket = winning_bets[start:start+size]
                start += size
                
                # award winnings
                for bet in bucket:
                    bet.player.money += tier

    def get_all_bets(self) -> Iterable["cupbot.models.bet.Bet"]:
        """this could probably be done with some advanced Relationship
        
        :rtype: list of Bet
        """
        query = cupbot.models.bet.Bet.query
        query = query.join(cupbot.models.bet.Player)
        query = query.filter(cupbot.models.bet.Player.game_id == self.game_id)
        return query.all()
        

class Leg(db.Model):
    """An individual leg within a game of Camel Cup"""
    __tablename__ = "leg"
    
    game_id = Column("game_id", Integer, db.ForeignKey("game.game_id"), primary_key=True)
    leg_index = Column("leg_index", Integer, primary_key=True)

    game = db.relationship("Game", 
                           backref=db.backref("legs", order_by="Leg.leg_index"))

    start_date = Column("start_date", TimeStampTZ, nullable=False)
    end_date = Column("end_date", TimeStampTZ)

    winner = Column("winner", String)
    loser = Column("loser", String)
    
    def __init__(self, game: Game, leg_index: int = 1, start_date: datetime = None):
        debug(f"creating leg {leg_index} for game #{game.game_id}")
        self.game = game
        self.leg_index = leg_index
        
        if start_date:
            self.start_date = start_date
        elif leg_index == 1:
            self.start_date = game.start_date
        else:
            raise ValueError("Must specify a start_date for any leg after the first")
    
    @property
    def finished(self):
        """True if this leg was completed"""
        return self.end_date != None
    
    def for_json(self):
        return {
            "leg_index": self.leg_index,
            "start_date": self.start_date,
            "end_date": self.end_date,
        }
    
    def __str__(self):
        return f"<Leg {self.leg_index} for game #{self.game_id}>"
        
