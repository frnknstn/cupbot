from flask import request

from sqlalchemy import Column, SmallInteger

import cupbot.database
from cupbot.database import db, NoResultFound


class Color(db.Model):
    __tablename__ = "color"
    
    red = Column("red", SmallInteger, primary_key=True, nullable=False, default=0)
    green = Column("green", SmallInteger, primary_key=True, nullable=False, default=0)
    blue = Column("blue", SmallInteger, primary_key=True, nullable=False, default=0)
    
    @classmethod
    def get_color(cls):
        c = cls.query.one()
        return c
    
    @classmethod
    def set_color(cls, r, g, b):
        c = cls.query.one()
        c.red, c.green, c.blue = r, g, b
        db.session.commit()
        return c
    
    def get_hex(self):
        """I am kind of guessing what format the frontend actually wants"""
        return "#%02x%02x%02x" % (self.red, self.green, self.blue)
        
