"""A player wot did a participation in a game"""

from logging import info, debug

from sqlalchemy import Column, String, Integer, UniqueConstraint

import cupbot.database
from cupbot.utils import now
from cupbot.database import db, TimeStampTZ, NoResultFound
from cupbot.exceptions import GameError, BetError
from cupbot.twitch import twitch_ext
import cupbot.models.game

STARTING_MONEY = 3


class Player(db.Model):
    __tablename__ = "player"
    
    player_id = Column("player_id", Integer, primary_key=True)
    twitch_opaque_id = Column("twitch_opaque_id", String, nullable=False)  # Twitch opaque user ID 
    game_id = Column("game_id", Integer, db.ForeignKey('game.game_id'), nullable=False)
    game = db.relationship("Game", backref="players")
    
    display_name = Column("display_name", String)
    money = Column("money", Integer, nullable=False)
    
    __table_args__ = (
        UniqueConstraint("twitch_opaque_id", "game_id"),
    )
    
    @classmethod
    def get_or_create(cls, twitch_opaque_id: str, game: "cupbot.models.game.Game", user_id: str = None):
        """get or create a player for this game"""
        need_create = False
        need_name = True
        try:
            player = cls.get_player(twitch_opaque_id, game)
            
            if not player.display_name and user_id:
                # prev name fetch must have failed 
                info("Player is missing a display name")
                player.display_name = twitch_ext.get_user_display_name(user_id)
                db.session.commit()
        except NoResultFound:
            # name us if possible
            if user_id:
                display_name = twitch_ext.get_user_display_name(user_id)
            else:
                display_name = None
            player = Player.create(twitch_opaque_id, game, display_name)
        
        return player
    
    @classmethod
    def get_player(cls, twitch_opaque_id: str, game: "cupbot.models.game.Game"):
        """get an existing player or raise NoResultFound"""
        game_id = game.game_id
        player = cls.query.filter(
            cls.twitch_opaque_id == twitch_opaque_id,
            cls.game_id == game_id
        ).one()
        debug(f"Found existing player {player.player_id} for user {twitch_opaque_id} in game {game_id}")
        return player

    @classmethod
    def create(cls, twitch_opaque_id: str, game: "cupbot.models.game.Game", display_name: str = None):
        game_id = game.game_id
        info(f"creating player {twitch_opaque_id} for game {game_id}")
        player = cls()
        player.twitch_opaque_id = twitch_opaque_id
        player.game_id = game.game_id
        player.display_name = display_name
        player.money = STARTING_MONEY
        game.player_count += 1
        db.session.add(player)
        db.session.commit()
        return player

    @property
    def name(self):
        """The final name that should be displayed to users
        
        This mostly just gives nameless users the name "Anonymous"
        """
        if self.display_name:
            return self.display_name
        else:
            return "Anonymous"
    
    def for_json(self):
        retval = {
            "player_id": self.player_id,
            "name": self.name,
            "score": self.money,
            "picks": [ x.for_json() for x in self.bets ],
            # some endpoints will also add "pick_cooldown": float,
        }
        return retval


class Bet(db.Model):
    """A bet by a player on a leg"""
    __tablename__ = "bet"
    
    bet_id = Column("bet_id", Integer, primary_key=True)
    
    player_id = Column("player_id", Integer, db.ForeignKey('player.player_id'), nullable=False)
    player = db.relationship("Player", backref="bets")
    
    leg_index = Column("leg_index", Integer, nullable=False)
    
    category = Column("category", String, nullable=False)
    camel = Column("camel", String, nullable=False)
    payout = Column("payout", Integer)  # £££ used for leg chits
    order = Column("order", Integer)    # what order your game winner bet was 
    
    date_created = Column("date_created", TimeStampTZ, nullable=False)
    
    CATEGORIES = ("leg win", "race win")
    STAKES = (5, 3, 2, 2)
    
    def __init__(self, player: Player, category: str, camel: str):
        game = player.game
        leg = game.current_leg
        if camel not in game.camel_colors:
            raise GameError("That camel is not in this race")
        
        if leg.finished:
            # the leg is over, can't bet on it
            raise BetError(f"Current leg has concluded")

        if category in ("leg win",): 
            # work out our stake
            chits = len(tuple(
                x for x in player.bets
                if x.category == category and x.leg_index == leg.leg_index
            ))
            debug(f"betting with {chits} chits already")
            
            try:
                self.payout = self.STAKES[chits]
                debug(f"chit is worth £{self.payout}")
            except IndexError:
                # no more chits available
                raise BetError(f"Max '{category}' picks reached")            
            
        elif category in ("race win"):
            # can only bet once per camel to win the race
            for bet in (x for x in player.bets if x.category == category):
                if bet.camel == camel:
                    raise BetError("Already picked this camel to win the race")
            
            # work out our order
            self.order = len(tuple(x for x in game.get_all_bets() if x.category == category)) + 1
            order = 0
            for bet in (x for x in game.get_all_bets() if x.category == category):
                order += 1
                self.order = order
            
            debug(f"chit has the serial number {self.order}")
        
        # populate us
        self.category = category
        self.camel = camel
        self.leg_index = leg.leg_index
        self.date_created = now()
        self.player = player
    
    @property
    def game(self):
        return self.player.game
    
    @property
    def game_id(self):
        return self.player.game_id
    
    def for_json(self):
        retval = {
            "camel": self.camel,
            "category": self.category,
            "leg_index": self.leg_index,
        }
        if self.category == "leg win":
            retval["value"] = self.payout
        elif self.category == "race win":
            retval["order"] = self.order
        return retval

