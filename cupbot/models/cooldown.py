from logging import info, debug
import datetime

from flask import request

from sqlalchemy import Column, String

import cupbot.database
from cupbot.database import db, IntegrityError, TimeStampTZ
import cupbot.utils


class Cooldown(db.Model):
    __tablename__ = "cooldown"
    
    user_id = Column("user_id", String, primary_key=True)
    context = Column("context", String, primary_key=True)
    date_created = Column("date_created", TimeStampTZ, nullable=False)
    expires = Column("expires", TimeStampTZ, nullable=False)
    
    def __init__(self, user_id: str, context: str, now: datetime.datetime, expires: datetime.datetime):
        self.user_id = user_id
        self.context = context
        self.date_created = now
        self.expires = expires
        
    def __str__(self):
        return f"<Cooldown {self.user_id} '{self.context}' at {self.expires.isoformat(' ')}>'"


def check_cooldown(user_id: str, context: str, duration: float) -> float:
    """checks and sets a cooldown. Will commit the transaction."""
    now = cupbot.utils.now()
    
    # primary check
    cooldown = Cooldown.query.filter(db.and_(
        Cooldown.user_id == user_id,
        Cooldown.context == context,
    )).order_by(Cooldown.expires.desc()).first()

    if cooldown:
        if cooldown.expires > now:
            remains = (cooldown.expires - now).total_seconds()
            info(f"{user_id} is on cooldown for '{context}' for another {remains:.2f} sec")
            return remains
        else:
            debug("Found expired cooldown: " + str(cooldown))
    
    # try to claim the cooldown
    # first, remove any expired claims
    Cooldown.query.filter(db.and_(
            Cooldown.user_id == user_id,
            Cooldown.context == context,
            Cooldown.expires <= now
    )).delete()
    
    # next, stake our claim
    expires = now + datetime.timedelta(seconds=duration)
    cd = Cooldown(user_id, context, now, expires)
    try:
        db.session.add(cd)
        db.session.flush()
    except IntegrityError as e:
        # conflict, simultaneous claim?
        info(f"A simultaneous query has claimed the {user_id} cooldown for {context}: {e}")
        raise
    else:
        # we have set the cooldown, go ahead
        db.session.commit()
        info(f"Created a {duration} second cooldown for {user_id} {context}")
        return 0

