"""Twitch Helper Functions"""

from typing import Dict, Any
from logging import info, warning
import traceback
import base64
import time

import requests
import jwt

from .exceptions import AuthenticationError
from .models.cooldown import check_cooldown

SERVER_TOKEN_DURATION = 30
BEARER_PREFIX = "Bearer "

INTERNAL_USER_ID = "X0"
BROADCAST_COOLDOWN = (INTERNAL_USER_ID, "pubsub", 5)
API_COOLDOWN = (INTERNAL_USER_ID, "api", 1)


class TwitchExt:
    def __init__(self):
        """Helper class for Twitch Extensions"""
        self.client_id = ""
        self.jwt_secret = b""
        self.owner_id = ""
    
    def configure(self, client_id: str, jwt_secret: str, owner_id: str, api_access_token: str):
        """Set up this helper"""
        self.client_id = client_id
        self.jwt_secret = base64.b64decode(jwt_secret)
        self.owner_id = owner_id
        self.api_access_token = api_access_token.encode("utf8")

    def verify_and_decode(self, request) -> Dict[str, Any]:
        """Verify and decode the Twitch JWT in the request's headers"""
        try:
            header = request.headers["Authorization"]
        except KeyError:
            raise AuthenticationError("No Authorization header in request")
        
        if not header.startswith(BEARER_PREFIX):
            raise AuthenticationError("Invalid Authorization header in request")
        
        token = header[len(BEARER_PREFIX):]
        
        try:
            payload = jwt.decode(token, self.jwt_secret, algorithms='HS256')
        except jwt.exceptions.DecodeError as e:
            traceback.print_exc()
            raise AuthenticationError("Invalid JWT supplied")
        
        return payload
    
    def make_server_token(self, channel_id: str = None) -> bytes:
        """Return a JWT for a channel"""
        payload = {
            "exp": int(time.time()) + SERVER_TOKEN_DURATION,
            # "channel_id": channel_id,
            "user_id": self.owner_id,  # extension owner ID
            "role": "external",
            "pubsub_perms": {"send": ['*']},
        }
        if channel_id:
            payload["channel_id"] = channel_id
        
        return jwt.encode(payload, self.jwt_secret, algorithm="HS256")
    
    def send_pubsub_broadcast(self, channel_id: str, message: str):
        """Send a channel broadcast via the Twitch PubSub API"""
        info(f"Sending broadcast PubSub message to channel {channel_id}")
        if check_cooldown(*BROADCAST_COOLDOWN) > 0:
            info("skipping broadcast, on cooldown")
            return 
        
        headers = {
            "Client-ID": self.client_id,
            "Authorization": BEARER_PREFIX.encode("utf8") + self.make_server_token(channel_id),
        }
        
        payload = {
            "message": message,
            "targets": ["broadcast"],
            "content_type": 'application/json',
        }
        
        r = requests.post(
            "https://api.twitch.tv/extensions/message/"+channel_id, 
            data=payload, 
            headers=headers,
        )
        
        info(f"PubSub responds: {r.status_code} {r.text}")
        
    def get_user_details(self, user_id: str):
        """Get a user's details, including user name. Must have that user's *real* ID (not opaque id)
        """
        
        info(f"Fetching user info for {user_id}")
        if check_cooldown(*API_COOLDOWN) > 0:
            warning("skipping user fetch, on cooldown")
            return

        headers = {
            "Client-ID": self.client_id,
            "Authorization": BEARER_PREFIX.encode("utf8") + self.api_access_token,
        }
        
        params = {
            "id": user_id
        }

        r = requests.get(
            "https://api.twitch.tv/helix/users",
            params=params,
            headers=headers,
        )

        info(f"API responds: {r.status_code} {r.text}")
        return r.json()

    def get_user_display_name(self, user_id: str) -> str:
        details = self.get_user_details(user_id)["data"][0]
        display_name = details.get("display_name", None)
        info(f"Twitch says user {user_id} is called '{display_name}'")
        return display_name


# singleton helper
twitch_ext = TwitchExt()
configure = twitch_ext.configure
