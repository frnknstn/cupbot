#!/usr/bin/env/python3
import logging

import cupbot

if __name__ == "__main__":
    retval = cupbot.create_app().run(host='0.0.0.0', port=8081, debug=True)
    # retval = cupbot.create_app().run(host='0.0.0.0', port=8081, use_reloader=False)
    logging.info("app exiting: " + str(retval))
