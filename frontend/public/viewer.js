var token = "";
var tuid = "";
var ebs = "";

const BACKEND = location.protocol + "//localhost:8081/"

// because who wants to type this every time?
var twitch = window.Twitch.ext;

// create the request options for our Twitch API calls
var requests = {
    color_cycle: {type: "POST", url: BACKEND + "color/cycle", success: updateBlock, error: logError},
    color_get: {type: "GET", url: BACKEND + "color/query", success: updateBlock, error: logError},

    game_get: {type: "GET", url: BACKEND + "game/query", success: updateGame, error: logError},

    start_game: {type: "POST", url: BACKEND + "game/create", success: updateGame, error: logError},
    end_leg: {type: "POST", url: BACKEND + "game/leg", success: updateGame, error: logError},
    end_game: {type: "POST", url: BACKEND + "game/leg", success: updateGame, error: logError},

    player_get: {type: "GET", url: BACKEND + "player/query", success: updatePlayer, error: logError},
    pick_leg: {type: "POST", url: BACKEND + "pick/leg",  success: updatePlayer, error: logError},
    pick_race: {type: "POST", url: BACKEND + "pick/race", success: updatePlayer, error: logError},
};

const GAME_STATES = [
    "start",    // no auth or anything yet
    "pregame",
    "game",
    "pick",
    "results",
]

let game_state = "start";
let game_data = null;
let player_data = null;

let leg_timer = new AutoTimer("#t_leg_duration");
let pick_timer = new AutoCountdown('#t_pick');

function set_game_state(new_state) {
    let old_state = "";

    if (!GAME_STATES.includes(new_state)) {
        twitch.rig.log("BAD GAME STATE " + new_state);
        return;
    } else {
        twitch.rig.log(`Game state is now '${new_state}'`)
        old_state = game_state;
        game_state = new_state;
    }

    // need to think of consistent approach for this js state machine design
    if (new_state === "pregame") {
        set_ooc_notice("Waiting for game to start...")

        // pre-query our state
        $.ajax(requests.game_get);
        $.ajax(requests.player_get);
        $.ajax(requests.color_get);
        // TODO: retry the gets


    } else if (new_state === "game") {
        set_ooc_notice("");

        // show the actual UI, starting with the main tab
        let tabs = $('div.tab');
        tabs.css("display", "block");
        tabs.each(function(index) {
            $(this).css("display", "none");
        });
        $('#main_tab').css("display", "block")
    }
}

function setAuth(token) {
    twitch.rig.log('Setting auth headers');
    Object.keys(requests).forEach((req) => {
        requests[req].headers = { 'Authorization': 'Bearer ' + token }
    });
}

twitch.onContext(function(context) {
    twitch.rig.log(context);
});

twitch.onAuthorized(function(auth) {
    twitch.rig.log('onAuthorized()')

    // save our credentials
    token = auth.token;
    tuid = auth.userId;
    setAuth(token);

    // start our state machine
    set_game_state("pregame")
});

function set_ooc_notice(msg) {
    // set a text status message for behind-the-scenes issues
    if (msg !== "") {
        let notice = $('#ooc_notice');
        notice.css('display', 'block');
        notice.text(msg);
    } else {
        // hide the notice
        $('#ooc_notice').css('display', 'none');
    }
}

function set_span(class_name, s) {
    // helper to set a span of a certain class to have some text.
    s = String(s);
    $('#'+class_name).innerText = s;
}

function updateBlock(hex) {
    twitch.rig.log('Updating block color ' + hex);
    $('#color').css('background-color', hex);
    $('#cycle').css('background-color', hex+"80");
}

function updateGame(data) {
    twitch.rig.log("Got game info " + JSON.stringify(data));

    let need_player_update = false;
    if (!game_data) {
        // our first game data
        if (player_data) {
            // we have pending player data to parse
            need_player_update = true;
        }
    }

    game_data = data;

    // render our data
    $('#g_leg_id').text(parseInt(data.current_leg.leg_index));
    $('#g_player_count').text(parseInt(data.player_count));

    leg_timer.setStart(Date.parse(data.current_leg.start_date));

    if (!data.finished) {
        $('#g_heading').text("Time to Camel Up!");
        leg_timer.start();
    } else {
        $('#g_heading').text("Game is over!");
    }

    // deferred updates
    if (need_player_update) {
        updatePlayer(player_data);
    }

    // change states
    if (game_state === "pregame") {
        // start the game already!
        set_game_state("game")
    }
}

function make_pick_div(pick) {
    // take a pick JSON object and return the HTML string representation for it
    if (pick.category === "leg win") {
        var text = "🐪£" + parseInt(pick.value);
    } else {
        var text = "🐪#" + parseInt(pick.order);
    }

    switch (pick.camel) {
        case "blue":
        case "green":
        case "orange":
        case "yellow":
        case "white":
            var color_class = 'camel_' + pick.camel;
            var title = pick.camel + " camel";
            break;
    }

    return ` <span class=\"camel_pick ${color_class}\" title=\"${title}\">${text}</span> `
}

function updatePlayer(data) {
    twitch.rig.log("Got player info " + JSON.stringify(data));

    player_data = data

    // update player info
    $('#p_score').text(parseInt(data.score));

    // do we have a game yet?
    // we need game data to make sense of our picks data
    if (!game_data) {
        player_data.pick_cooldown = 0;
        return
    }

    let pick_cooldown = data.pick_cooldown * 1000;
    if (pick_cooldown > 0) {
        pick_timer.setTarget(pick_cooldown);
    }

    // update our list of picks
    let leg_picks = $('#b_leg_picks');
    let race_picks = $('#b_race_picks');
    leg_picks.empty();
    race_picks.empty();

    data.picks.forEach(function(item, index) {
        let html = make_pick_div(item);
        if (item.category === "leg win") {
            if (item.leg_index === game_data.current_leg.leg_index) {
                // leg pick was for not for another race leg
                leg_picks.append(html);
            }
        } else {
            race_picks.append(html);
        }
    })
}

function logError(xhr, error, status) {
  twitch.rig.log('EBS request returned '+status+' ('+error+'): ' + xhr.responseText);
}

function logSuccess(hex, status) {
  // we could also use the output to update the block synchronously here,
  // but we want all views to get the same broadcast response at the same time.
  twitch.rig.log('EBS request returned '+hex+' ('+status+')');
}

function click_start_game() {
    twitch.rig.log("click_start_game()")
    $.ajax(requests.start_game);
}

function custom_request(request, data) {
    // make an AJAX call with custom copy of a request with extra json data
    let new_request = {
        ...request,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(data),
    };
    $.ajax(new_request);
}

function click_end_leg() {
    twitch.rig.log("click_end_leg()")
    let data = {
        winner: "blue",
        second: "green",
        loser: "white",
        final: false,
    }
    custom_request(requests.end_leg, data);
}

function click_end_game() {
    twitch.rig.log("click_end_game()")
    let data = {
        winner: "blue",
        second: "green",
        loser: "white",
        final: true,
    }
    custom_request(requests.end_game, data);
}

function pick_leg_winner(camel) {
    custom_request(requests.pick_leg, {winner:camel})
}

function pick_race_winner(camel) {
    custom_request(requests.pick_race, {winner:camel})
}

$(function() {
    // when we click the cycle button
    $('#cycle').click(function() {
        if(!token) { return twitch.rig.log('Not authorized'); }
        twitch.rig.log('Requesting a color cycle');
        $.ajax(requests.color_cycle);
    });

    // listen for incoming broadcast message from our EBS
    twitch.listen('broadcast', function (target, contentType, color) {
        twitch.rig.log('Received broadcast color');
        updateBlock(color);
    });
});
