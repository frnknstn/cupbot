class AutoTimer {
    constructor(element_id, start=0) {
        // draw and update a cooldown or timer in a specified element.
        this.interval = null;
        this.element_id = element_id
        this.running = false;
        this.start_time = start;
        this.setStart(start);
    }

    setStart(start) {
        // reset or change our start time
        if (start === 0) {
            start = Date.now();
        }
        this.start_time = start
        $(this.element_id).css("display", "inline");
        this.tickUp();
    }

    start() {
        // reset our counter
        if (this.start === 0){
            this.start = Date.now();
        }
        this.running = true;
        this.tickUp();
        this.interval = setInterval(this.tickUp.bind(this), 1000);
    }

    stop() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.running = false;
        // $(this.element_id).css("display", "none");
    }

    tickUp() {
        // update our element display
        let now = Date.now();
        let duration = (now - this.start_time) / 1000;

        let m = Math.floor(duration / 60);
        let s = Math.floor(duration) % 60;

        $(this.element_id).text(m.toString() + ":" + s.toString().padStart(2, '0'));
    }
}


class AutoCountdown {
    constructor(element_id, target=0) {
        // draw and update a cooldown or timer in a specified element.
        this.interval = null;
        this.interval_timeout = 0;
        this.element_id = element_id
        this.running = false;
        this.setTarget(target);
    }

    setTarget(target) {
        // reset or change our countdown target
        this.target = Date.now() + target;
        this.stop();
        if (target > 0) {
            this.start();
            $(this.element_id).css("display", "inline");
        }
    }

    start() {
        // reset our counter
        this.running = true;
        this.tickUp();
        this.makeInterval()
    }

    stop() {
        if (this.interval) {
            clearInterval(this.interval);
            this.interval = null;
        }
        this.running = false;
    }

    makeInterval(timeout=0) {
        // set the tick rate for our timer
        if (this.interval) {
            clearInterval(this.interval);
        }
        if (timeout === 0) {
            timeout = Math.min(
                73,
                this.target - Date.now()
            );
        }
        this.interval_timeout = timeout;
        this.interval = setInterval(this.tickUp.bind(this), timeout);
    }

    tickUp() {
        // update our element display
        let remaining_ms = (this.target - Date.now());
        let remaining = remaining_ms / 1000;

        if (remaining <= 0) {
            // Done!
            this.stop();
            $(this.element_id).text("now.");
        }  else if (remaining <= 10) {
            // short time display mode
            if (remaining_ms <= this.interval_timeout) {
                // avoid overshooting by too much
                this.makeInterval(remaining_ms);
            }
            $(this.element_id).text(remaining.toFixed(2) + "sec");
        } else {
            // m:SS display
            let m = Math.floor(remaining / 60);
            let s = Math.floor(remaining) % 60;

            $(this.element_id).text(m.toString() + ":" + s.toString().padStart(2, '0'));
        }
    }
}
