## CupBot #

Twitch extension for audience participation in streamed games of *Camel Up*.

Or at least, it will be. Right now it is an alternative Python backend implementation for the Twitch color change example code.

### How betting works: #

Can bet every 5 minutes.

Leg winner: Can bet max 4 times per leg. Each betting chit is worth less: £5, £3, £2, £2. If the camel wins you get the value. If the camel comes second, you get £1. Otherwise you lose £1.

Overall winner: Can bet once per leg and once per camel. All bets on the winning camel are ordered and divided up evenly into buckets, with at least one player per bucket: £8, £5, £3, £2, £1. Every bet on a losing camel costs £1.

### To do: #

* game lifecycle management
  * create a game
  * end a game
  * end a leg
* game state management
  * send game state
  * advance game state
  * states:
    * pre-game?
    * in a leg?
    * post-game?  
* gaming backend
  * odds and payouts
* player management
  * join a game
  * bet on a leg winner
  * bet on a leg loser
  * bet on an overall winner
  * bet on an overall loser
* leaderboards
  